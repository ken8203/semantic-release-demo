BINARY_NAME=demo
DESTINATION=./builds/${BINARY_NAME}

.PHONY: release
release: compile-darwin compile-linux compile-windows

compile-windows:
	CGO_ENABLED=0 GOOS=windows GOARCH=386 go build -ldflags "-X main.version=$(VERSION)" -o ${DESTINATION}.exe ./cmd/demo

compile-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=$(VERSION)" -o ${DESTINATION}-linux-amd64 ./cmd/demo
	CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -ldflags "-X main.version=$(VERSION)" -o ${DESTINATION}-linux-arm64 ./cmd/demo

compile-darwin:
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -ldflags "-X main.version=$(VERSION)" -o ${DESTINATION} ./cmd/demo
