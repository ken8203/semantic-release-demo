## [1.1.0](https://gitlab.com/ken8203/semantic-release-demo/compare/v1.0.0...v1.1.0) (2020-12-24)


### Chores

* **ci:** modify presetConfig of release-notes-generator ([779440e](https://gitlab.com/ken8203/semantic-release-demo/commit/779440ecbf8725385c1cf6e7df0646053d2ab4b5))


### Features

* enrich cmd info ([d1643b7](https://gitlab.com/ken8203/semantic-release-demo/commit/d1643b775efcb6b1bf223cd6cf525faee7465d23))

## 1.0.0 (2020-12-24)


### Features

* cmd ([e0810b7](https://gitlab.com/ken8203/semantic-release-demo/commit/e0810b78e3d66e92c9b9970958e267b1500c91b4))
