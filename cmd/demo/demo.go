package main

import (
	"fmt"
	"time"
)

var version string

func main() {
	fmt.Println("Version:", version)
	fmt.Println("Time:", time.Now().Format("2006-01-02 15:04:05"))
}
